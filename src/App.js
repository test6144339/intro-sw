import logo from './logo.svg';
import './App.css';
import gato from '../src/assets/gato.jpg';
import gato1 from '../src/assets/gato1.jpg';
import gato2 from '../src/assets/gato2.jpg';
import gato3 from '../src/assets/gato3.jpg';
import gato4 from '../src/assets/gato4.jpg';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div>
      <img src={gato} alt='gato'></img>
      <img src={gato1} alt='gato1'></img>
      <img src={gato2} alt='gato2'></img>
      <img src={gato3} alt='gato3'></img>
      <img src={gato4} alt='gato4'></img>
      </div>
    </div>
  );
}

export default App;
