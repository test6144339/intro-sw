//console.log('SW: Hello :)')

//Responder con lo que venga en la aplicacion

/*self.addEventListener('fetch', event => {
    event.respondWith(fetch(event.request))
})*/

//imprimir el evento
/*self.addEventListener('fetch', event => {
  console.log(event)
})*/

//bloquear el archivo favicon.
/*
self.addEventListener('fetch', event =>{
    if(event.request.url.includes('favicon.ico')){
        event.respondWith(null)
    } else {
        event.respondWith(fetch(event.request))
    }
 })*/

 //Modificando la respuesta

/* self.addEventListener('fetch', event =>{
   const oflineResponse = new Response(`
    Bienvenido a mi pagina web
    Disculpa pero debes tener internet
    `)
   const response = fetch (event.request)
   .catch(() => oflineResponse)

   event.respondWith(response)
  })*/

  /*self.addEventListener('fetch', event =>{
    const oflineResponse = new Response(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>React App</title>
    </head>
    <body>
    <h1>Ofline Mode</h1>
    </body>
    </html>
     `, {
        headers: {
            'Content-Type':'text/html'
        }
     })
    const response = fetch (event.request)
    .catch(() => oflineResponse)
 
    event.respondWith(response)
   })*/

   const CACHE_NAME = 'cache.1'
   const CACHE_DYNAMIC = 'dynamic-v1'
   const CACHE_STATIC = 'static-v1'
   const CACHE_INMUTABLE = 'inmutable-v1'

   const limpiarCache= (cacheName, numberItem) =>{
    caches.open(cacheName)
    .then(cache => {
        cache.keys()
        .then(keys => {
            if(keys.length > numberItem){
                cache.delete(keys[0])
                .then(limpiarCache(cacheName, numberItem))
            }
        })
    })
   }
   self.addEventListener('install' ,function (event){

    const cachePromise = caches.open(CACHE_STATIC).then(function(cache){
        return cache.addAll([
            '/',
            '/index.html',
            '/js/app.js',
            '/sw.js',
            'static/js/bundle.js',
            'favicon.ico'
        ])
    })

    //event.waitUntil(cachePromise)

        const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function(cache){
            return cache.addAll([
                'https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap'
            ])
        })
            event.waitUntil(Promise.all([cachePromise, cacheInmutable]))
    })

    
   self.addEventListener('fetch', function (event){

    const respuesta = caches.match(event.request).then(response => {
        if(response) return response
        return fetch(event.request).then(newResponse =>{
            caches.open(CACHE_DYNAMIC).then(cache => {
                cache.put(event.request, newResponse)
            })
            limpiarCache(CACHE_DYNAMIC, 9)
            return newResponse.clone()
        })
    })
    event.respondWith(respuesta)
   })

